#!/bin/bash
# LOG version 1.7.5
# author: Keper#6769
# Thanks for the help: _kamiya#2088, Norz3n#5686, Rus_Nor#5826, Elifian#0761
# YouTube: https://www.youtube.com/c/Lintech8
# Codeberg: https://codeberg.org/Lintech
# Github: https://github.com/Lintech-1/arch-script/ (old)
#-------------------------------------------------------------------------------------- 
# - Изменена функция добавления репозитория multilib
# - Полностью переписана установка кастомных ядер (по желанию пользователя)
# - Добавлена установка микрокода для поддержки процессоров AMD и Intel
# - Изменена система установки видеодрайверов
# - Изменена система обнаружение рабочей оболочки
# - Исправлено добавление Zram для увеличения производительности
# - Реализовано добавление параметров для makepkg при сборке пакетов
# - Реализована установка Clang и LLVM компиляторами по умолчанию (по желанию пользователя)
# - Исправлена установка ZSH и прочих пакетов для работы с ZSH
# - Изменена логика установки AUR-помощника
#--------------------------------------------------------------------------------------

cas(){
  clear
  REPLY=
}

key_updates(){
  echo "Обновление ключей"
  sleep 3
  sudo pacman -Sy archlinux-keyring
}

system_update(){
  echo "Обновление системы"
  sleep 3
  sudo pacman -Syu --noconfirm
}

repositories(){
  echo "multilib и chaotic-aur"
  sleep 3
  if grep "[[]" /etc/pacman.conf | grep "multilib" | grep -v "multilib-testing" | grep -v "#" > /dev/null; then
    echo "Репозиторий уже есть!"
  else
    sudo sed -i '$ a \\n[multilib]\nInclude = /etc/pacman.d/mirrorlist' /etc/pacman.conf
  fi

  if grep "chaotic-aur" /etc/pacman.conf; then
    echo "Chaotic-AUR уже добавлен!"
  else
    sudo pacman-key --recv-key FBA220DFC880C036 --keyserver keyserver.ubuntu.com
    sudo pacman-key --lsign-key FBA220DFC880C036
    sudo pacman -U --noconfirm 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'
    sudo sed -i '$ a \\n[chaotic-aur]\nInclude = /etc/pacman.d/chaotic-mirrorlist' /etc/pacman.conf
  fi
}

editors(){
    echo "Какой редактор вы хотите установить? (можно выбрать несколько)
1) nano
2) vim
3) neovim
4) vscodium
5) VS CODE
Чтобы ничего не устанавливать просто нажмите ENTER"
    read EDITOR
    if [[ $EDITOR = [1] ]]; then
        EDITOR1=nano
    elif [[ $EDITOR = [2] ]]; then
        EDITOR2=vim
    elif [[ $EDITOR = [3] ]]; then
        EDITOR3=neovim
    elif [[ $EDITOR = [4] ]]; then
        EDITOR4=vscodium
    elif [[ $EDITOR = [5] ]]; then
        EDITOR5=visual-studio-code-bin
    elif [[ $EDITOR = "" ]]; then
        :
    else
        echo "Введено некорректное значение, повторите попытку"
        editors
    fi

    EDITOR0="$EDITOR1 $EDITOR2 $EDITOR3 $EDITOR4 $EDITOR5"
    if [[ $EDITOR0 =~ [A-Za-z] ]]; then
        sudo pacman -Sy --needed $EDITOR0
    elif [[ $EDITOR0 = 0 ]]; then
        :
    fi
}

soft(){
  echo "Установка софта"
  sleep 3
  sudo pacman -Sy --needed linux linux-headers linux-firmware linux-firmware-whence discord telegram-desktop m4 git wget curl mpv neofetch flameshot squashfs-tools python-gobject python-setuptools noto-fonts-cjk noto-fonts-emoji ttf-joypixels ttf-dejavu ttf-font-awesome inxi libmtp ccache fuse3 mtpfs gvfs gvfs-mtp gpm

  echo "Установка микрокода для процесора..."
    
  cpu_model_name=$(cat /proc/cpuinfo | grep vendor | cut -c 13-24 | uniq)
  
  if [[ $cpu_model_name = "GenuineIntel" ]]; then
      sudo pacman -Sy --needed intel-ucode
  fi
  if [[ $cpu_model_name = "AuthenticAMD" ]]; then
      sudo pacman -Sy --needed amd-ucode
  fi
}

package_manager(){
  echo "Вы хотите установить yay?
1) Да
Если нет, то установится pikaur
2) Нет"
  echo -n "Введите цифру: "
  read AUR_HELPER
  if [[ "$AUR_HELPER" == 1 ]]; then
    cd /home/$USER/
    mkdir yay-bin
    cd file
    sudo pacman -S --needed git base-devel
    git clone https://aur.archlinux.org/yay-bin.git
    cd yay-bin
    makepkg -si

    aur_install = "yay"
  elif [[ "$AUR_HELPER" == 2 ]]; then
    cd /home/$USER/
    mkdir pikaur
    cd file
    sudo pacman -S --needed base-devel git
    git clone https://aur.archlinux.org/pikaur.git
    cd pikaur
    makepkg -fsri

    aur_install = "pikaur"
  fi
}

video_drivers_install(){
  # Проверка если aur_install возвращается пустое значение то вызвать функцию package_manager
  if [[ $aur_install = "" ]]; then
    package_manager
  fi
  echo "Установка видео драйверов"
  sleep 3
  nvidia_desktop_driver_install(){
    nvidia_series(){
      Tesla1=(G[0-9]*)
      Tesla2=(GT[0-9]*)
      Tesla3=(MCP[0-9]*)
      Fermi=(GF[0-9]*)
      Kepler=(GK[0-9]*)
      Maxwell=(GM[0-9]*)
      Pascal=(GP[0-9]*)
      Volta=(GV[0-9]*)
      Turing=(TU[0-9]*)
      Ampere=(GA[0-9]*)
      [[ $(lspci | grep VGA | 3D | sed -rn 's/.*(G[0-9]*).*/\1/p') == $Tesla1 ]] && NVIDIA=Tesla
      [[ $(lspci | grep VGA | 3D | sed -rn 's/.*(GT[0-9]*).*/\1/p') == $Tesla2 ]] && NVIDIA=Tesla
      [[ $(lspci | grep VGA | 3D | sed -rn 's/.*(MCP[0-9]*).*/\1/p') == $Tesla3 ]] && NVIDIA=Tesla
      [[ $(lspci | grep VGA | 3D | sed -rn 's/.*(GF[0-9]*).*/\1/p') == $Fermi ]] && NVIDIA=Fermi
      [[ $(lspci | grep VGA | 3D | sed -rn 's/.*(GK[0-9]*).*/\1/p') == $Kepler ]] && NVIDIA=Kepler
      [[ $(lspci | grep VGA | 3D | sed -rn 's/.*(GM[0-9]*).*/\1/p') == $Maxwell ]] && NVIDIA=Maxwell
      [[ $(lspci | grep VGA | 3D | sed -rn 's/.*(GP[0-9]*).*/\1/p') == $Pascal ]] && NVIDIA=Pascal
      [[ $(lspci | grep VGA | 3D | sed -rn 's/.*(GV[0-9]*).*/\1/p') == $Volta ]] && NVIDIA=Volta
      [[ $(lspci | grep VGA | 3D | sed -rn 's/.*(TU[0-9]*).*/\1/p') == $Turing ]] && NVIDIA=Turing
      [[ $(lspci | grep VGA | 3D | sed -rn 's/.*(GA[0-9]*).*/\1/p') == $Ampere ]] && NVIDIA=Ampere
    }
    nvidia_series
    
    if [[ $NVIDIA == Tesla && $aur_install = "" ]]; then
      echo "340-ые драйвера отсувствуют в chaotic-aur!"
    elif [[ $NVIDIA == Tesla && $aur_install != "" ]]; then
      $aur_install -S nvidia-340xx-dkms nvidia-340xx-utils nvidia-340xx-settings
    elif [[ $NVIDIA == Fermi ]]; then
      $aur_install -S nvidia-390xx lib32-opencl-nvidia-390xx lib32-nvidia-390xx-utils nvidia-390xx-settings
    elif [[ $NVIDIA == Kepler ]]; then
      $aur_install -S nvidia-470xx-dkms nvidia-470xx-utils lib32-nvidia-470xx-utils nvidia-470xx-settings vulkan-icd-loader lib32-vulkan-icd-loader lib32-opencl-nvidia-470xx opencl-nvidia-470xx libxnvctrl-470xx
    elif [[ $NVIDIA == Maxwell || $NVIDIA == Pascal || $NVIDIA == Volta || $NVIDIA == Turing || $NVIDIA == Ampere ]]; then
      $aur_install -S nvidia-dkms nvidia-utils lib32-nvidia-utils nvidia-settings vulkan-icd-loader lib32-vulkan-icd-loader lib32-opencl-nvidia opencl-nvidia libxnvctrl
    fi

     echo "Прописывание модулей nvidia в /etc/mkinitcpio.conf"

    nvidia_modules_array=("nvidia" "nvidia_modeset" "nvidia_uvm" "nvidia_drm")

    for modules in "${nvidia_modules_array[@]}"; do
      if [[ $(grep -v "#" /etc/mkinitcpio.conf | grep "MODULES=(.*)" | grep "$modules") ]]; then  
          echo "Модуль $modules уже прописан!"
      elif [[ $(grep -v "#" /etc/mkinitcpio.conf | grep "MODULES=(.*)" | grep $modules) != $modules ]]; then
          sudo sed -i 's|MODULES=(.*[^)]|& '"$modules"'|' test.txt
          echo "Модуль $modules удачно прописан!"
      fi
    done

    sudo mkinitcpio -P

    echo "Настройка nvidia-drm..."

    grub_additional_param="nvidia-drm.modeset=1"

    if [[ $(grep 'GRUB_CMDLINE_LINUX_DEFAULT="*"' /etc/default/grub | grep "$grub_additional_param") ]]; then  
            echo "Параметр $grub_additional_param уже прописан!"
    elif [[ $(grep 'GRUB_CMDLINE_LINUX_DEFAULT="*"' /etc/default/grub | grep $grub_additional_param) != $grub_additional_param ]]; then
        sudo sed -i 's|GRUB_CMDLINE_LINUX_DEFAULT="[^"]*|& '"$grub_additional_param"'|' /etc/default/grub
        echo "Параметр $grub_additional_param удачно прописан!"
    fi

    sudo grub-mkconfig -o /boot/grub/grub.cfg
  }

  nvidia_hybrid_driver_install(){
    nvidia_desktop_driver_install

    # Если lspci | grep "3D" | sed -rn 's/.(Intel)./\1/p' возвращает Intel или lspci | grep "3D" | sed -rn 's/.(AMD)./\1/p' то вызывается проверка если NVIDIA = Tesla или NVIDIA = Fermi то устанановить Bumblebee. Если NVIDIA = Kepler или NVIDIA = Maxwell или NVIDIA = Pascal или NVIDIA = Volta или NVIDIA = Turing или NVIDIA = Ampere то установить nvidia-prime.
    if [[ $(lspci | grep "3D" | sed -rn 's/.*(Intel).*/\1/p') == Intel ]] || [[ $(lspci | grep "3D" | sed -rn 's/.*(AMD).*/\1/p') == AMD ]]; then
      if [[ $NVIDIA == Tesla || $NVIDIA = Fermi ]]; then
        $aur_install -S bumblebee

        echo "Настройка bumblebee..."

        echo "Добавление пользователя в группу bumblebee..."

        sudo usermod -a -G bumblebee $USER

        echo "Запуск службы bumblebee..."

        sudo systemctl enable bumblebeed.service

        echo "Bumblebee установлен!"
      elif [[ $NVIDIA == Kepler || $NVIDIA == Maxwell || $NVIDIA == Pascal || $NVIDIA == Volta || $NVIDIA == Turing || $NVIDIA == Ampere ]]; then
        $aur_install -S nvidia-prime
      fi
    fi
  }

  amd_driver_install(){
    echo "Внимание! Данный скрипт расчитывается на тех у кого более свежие видеокарты с поддержкой AMDGPU. Скрипт просто пропустит выполнение если видеокарты не поддерживают AMDGPU."

    echo "Установка драйверов для видеокарт AMD..."

    # Если lspci | sed "s/ {2,10}/ /g" | grep "Kernel driver in use: " | cut -c 24-31) возвращает radeon, то написать "Данный скрипт не поддерживает видеокарты с драйвером radeon".
    if [[ $(lspci | sed "s/ {2,10}/ /g" | grep "Kernel driver in use: " | cut -c 24-31) == radeon ]]; then
      echo "Данный скрипт не поддерживает видеокарты с драйвером radeon."
    else
      $aur_install -S xf86-video-amdgpu vulkan-radeon amdvlk lib32-vulkan-radeon lib32-amdvlk libva-mesa-driver lib32-libva-mesa-driver vulkan-icd-loader vulkan-mesa-layers
    fi
  }

  intel_driver_install(){
    echo "Внимание! Данный скрипт расчитывается на тех у кого более свежие видеочипы Intel с поддержкой Vulkan. Скрипт установит пакеты внезависимости от поддержки Vulkan."

    echo "Установка драйверов для видеокарт Intel..."

    $aur_install -S vulkan-intel vulkan-icd-loader vulkan-mesa-layers
  }

  if [[ $(lspci | grep VGA | sed -rn 's/.*(NVIDIA).*/\1/p') = NVIDIA ]]; then
    nvidia_desktop_driver_install
  elif [[ $(lspci | grep 3D | sed -rn 's/.*(NVIDIA).*/\1/p') = NVIDIA ]]; then
    nvidia_hybrid_driver_install
  elif [[ $(lspci | grep VGA | 3D | sed -rn 's/.*(AMD).*/\1/p') = AMD ]]; then
    amd_driver_install
  elif [[ $(lspci | grep VGA | 3D |  sed -rn 's/.*(Intel).*/\1/p') = Intel ]]; then
    intel_driver_install
  fi
}

kernel(){

  lqx_kernel_install(){
    echo "Добавление репозитория для ядра LQX"

    sudo pacman-key --keyserver hkps://keyserver.ubuntu.com --recv-keys 9AE4078033F8024D
    sudo pacman-key --lsign-key 9AE4078033F8024D

    sudo sed -i '$ a \\n[liquorix]\nServer = https://liquorix.net/archlinux/$repo/$arch' /etc/pacman.conf

    echo "Установка ядра LQX"
    sudo pacman -Sy --needed linux-lqx linux-lqx-headers

    echo "Установка ядра LQX завершена!"
  }

  xanmod_kernel_install(){
    echo "Выберите версию ядра:
    1) Edge
    2) LTS"
    echo -n "Введите цифру: "
    read XANMOD_KERNEL

    if [[ $XANMOD_KERNEL == 1 ]]; then
      echo "Установка ядра XANMOD Edge"
      sudo pacman -Sy --needed linux-xanmod-edge linux-xanmod-edge-headers
    elif [[ $XANMOD_KERNEL == 2 ]]; then
      echo "Установка ядра XANMOD LTS"
      sudo pacman -Sy --needed linux-xanmod-lts linux-xanmod-lts-headers
    fi
  }

  tkg_kernel_install(){
    echo "Выберите версию ядра:
    1) PDS
    2) BMQ
    3) СFS
    4) PDS-LTS
    5) BMQ-LTS
    6) СFS-LTS"

    echo -n "Введите цифру: "
    read TKG_KERNEL

    if [[ $TKG_KERNEL == 1 ]]; then
      echo "Установка ядра TKG PDS"
      sudo pacman -Sy --needed linux-tkg-pds linux-tkg-pds-headers
    elif [[ $TKG_KERNEL == 2 ]]; then
      echo "Установка ядра TKG BMQ"
      sudo pacman -Sy --needed linux-tkg-bmq linux-tkg-bmq-headers
    elif [[ $TKG_KERNEL == 3 ]]; then
      echo "Установка ядра TKG СFS"
      sudo pacman -Sy --needed linux-tkg-cfs linux-tkg-cfs-headers
    elif [[ $TKG_KERNEL == 4 ]]; then
      echo "Установка ядра TKG PDS-LTS"
      sudo pacman -Sy --needed linux-lts-tkg-pds linux-lts-tkg-pds-headers
    elif [[ $TKG_KERNEL == 5 ]]; then
      echo "Установка ядра TKG BMQ-LTS"
      sudo pacman -Sy --needed linux-lts-tkg-bmq linux-lts-tkg-bmq-headers
    elif [[ $TKG_KERNEL == 6 ]]; then
      echo "Установка ядра TKG СFS-LTS"
      sudo pacman -Sy --needed linux-lts-tkg-cfs linux-lts-tkg-cfs-headers
    fi
  }

  echo "Вы хотите установить кастомное ядро?
1 - Yes
Если не хотите - не будет устанавливаться
2 - No"
  echo -n "Введите цифру: "
  read KERNEL
  if [[ "$KERNEL" = 1 ]]; then
    echo "Выберите ядро:
1 - Linux-zen
2 - Linux-lqx
3 - Linux-xanmod-*
4 - Linux-tkg-*"
    echo -n "Введите цифру: "
    read KERNEL_CHOICE
    # Проверка установлен ли драйвер Nvidia, если lspci  -v -s  $(lspci | grep ' VGA ' | cut -d" " -f 1) | sed "s/ {2,10}/ /g" | grep "Kernel driver in use: " | cut -c 24-31) возвращает nouveau то вызвать функцию установки драйвера Nvidia
    if [[ "$(lspci | sed "s/ {2,10}/ /g" | grep "Kernel driver in use: " | cut -c 24-31)" == "nouveau" ]]; then
      video_drivers_install
    fi
    if [[ $KERNEL_CHOICE =~ 1 ]]; then
        sudo pacman -Sy --needed linux-zen linux-zen-headers
    elif [[ $KERNEL_CHOICE =~ 2 ]]; then
        if [[ $NVIDIA == Tesla || $NVIDIA == Fermi ]]; then
            echo "Ваш видеодрайвер не поддерживает ядро Liquorix"
        elif [[ $NVIDIA == Kepler || $NVIDIA == Maxwell || $NVIDIA == Pascal || $NVIDIA == Volta || $NVIDIA == Turing || $NVIDIA == Ampere ]]; then
            lqx_kernel_install
        elif [[ $(lspci | grep VGA | sed -rn 's/.*(AMD).*/\1/p') || $(lspci | grep VGA | sed -rn 's/.*(Intel).*/\1/p') ]]; then
            lqx_kernel_install
        fi
    elif [[ $KERNEL_CHOICE =~ 3 ]]; then
        if [[ $NVIDIA == Tesla || $NVIDIA == Fermi ]]; then
            echo "Ваш видеодрайвер не поддерживает ядро XanMod"
        elif [[ $NVIDIA == Kepler || $NVIDIA == Maxwell || $NVIDIA == Pascal || $NVIDIA == Volta || $NVIDIA == Turing || $NVIDIA == Ampere ]]; then
            xanmod_kernel_install
        elif [[ $(lspci | grep VGA | sed -rn 's/.*(AMD).*/\1/p') || $(lspci | grep VGA | sed -rn 's/.*(Intel).*/\1/p') ]]; then
            xanmod_kernel_install
        fi
    elif [[ $KERNEL_CHOICE =~ 4 ]]; then
        if [[ $NVIDIA == Tesla || $NVIDIA == Fermi ]]; then
            echo "Ваш видеодрайвер не поддерживает ядро XanMod"
        elif [[ $NVIDIA == Kepler || $NVIDIA == Maxwell || $NVIDIA == Pascal || $NVIDIA == Volta || $NVIDIA == Turing || $NVIDIA == Ampere ]]; then
            tkg_kernel_install
        elif [[ $(lspci | grep VGA | sed -rn 's/.*(AMD).*/\1/p') || $(lspci | grep VGA | sed -rn 's/.*(Intel).*/\1/p') ]]; then
            tkg_kernel_install
        fi
    fi


      echo "Обновление initramfs..."
      sleep 2
      sudo mkinitcpio -P
      echo "Обновление записей GRUB..."
      sleep 2
      sudo grub-mkconfig -o /boot/grub/grub.cfg
  fi
  echo "Вы хотите удалить стоковое ядро?
1 - Да
Если не хотите - не будет удалятся
2 - Нет"
  echo -n "Введите цифру: "
  read KERNEL_DEL
  if [[ "$KERNEL_DEL" = 1 ]]; then
    sudo pacman -Rdd linux linux-headers
  fi
}

DE(){
  echo "Обнаружение оболочки..."

  # Если $DESKTOP_SESSION=plasma то установить ПО для Плазмы, если равно Gnome, установить ПО для Gnome, если равно XFCE то установить ПО для XFCE, если равно Cinnamon то установить ПО для Cinnamon, если равно i3 или bspwm то установить ПО для них
  if [[ $DESKTOP_SESSION = plasma ]]; then
    echo "Устанавливаю ПО для KDE Plasma..."
    sudo pacman -Sy --needed kde-system kde-utilities gwenview

  elif [[ $DESKTOP_SESSION = gnome ]]; then
    echo "Устанавливаю ПО для GNOME..."
    sudo pacman -Sy --needed gnome-shell-extensions gnome-tweaks gnome-disks file-roller eog ttf-roboto ttf-roboto-mono xdg-user-dirs-gtk

  elif [[ $DESKTOP_SESSION = xfce ]]; then
    echo "Устанавливаю ПО для XFCE..."
    sudo pacman -Sy --needed xfce4-clipman-plugin xfce4-xkb-plugin thunar-archive-plugin pavucontrol mousepad viewnior redshift

    xfconf-query --channel thunar --property /misc-exec-shell-scripts-by-default --create --type bool --set true && thunar -q

  elif [[ $DESKTOP_SESSION = cinnamon ]]; then
    echo "Устанавливаю ПО для Cinnamon..."
    sudo pacman -Sy --needed xed xviewer nemo-fileroller nemo-preview nemo-terminal blueberry libcanberra xdg-dbus-proxy xdg-desktop-portal-gtk xdg-user-dirs xdg-user-dirs-gtk xdg-utils p7zip unrar unace lrzip squashfs-tools gst-plugins-good poppler-data mintlocale

  elif [[ $DESKTOP_SESSION = i3 || $DESKTOP_SESSION = bspwm ]]; then
    echo "Устанавливаю ПО для i3 или bspwm..."
    sudo pacman -Sy --needed lightdm-gtk-greeter xorg-xrdb file-roller rofi redshift vte3 pavucontrol blueman nemo cinnamon-translations picom qview parcellite nitrogen polkit lxqt-policykit

    cp additions/picom.conf /home/$USER/.config/picom.conf
    sudo groupadd plugdev
    sudo gpasswd -a $USER plugdev
    sudo usermod -a -G plugdev $USER
  fi

  echo "Прописывание lxpolkit в конфигурации..."
    if [ -e $HOME/.config/bspwm/bspwmrc ]; then
      sed '2a\lxqt-policykit-agent &' $HOME/.config/bspwm/bspwmrc
      
      elif [ -e $HOME/.config/i3/config ]; then
      echo "\\nexec --no-startup-id lxqt-policykit-agent" >> $HOME/.config/i3/config
    fi
}

services(){
  echo "Вы хотите установить cлужбы для улучшения прозводительности?
1 - Yes
Если не хотите, то не будут установлены
2 - No"
  echo -n "введите цифру: "
  read OPTIMIZATION_SERVICES
  if [[ "$OPTIMIZATION_SERVICES" == 1 ]]; then
    sudo pacman -Sy --needed --noconfirm ananicy-cpp ananicy-rules-git nohang irqbalance rng-tools
    echo "Запуск служб - Ananicy: Автоматическое назначение высокого приоритета приложениям"
    systemctl enable --now ananicy-cpp && systemctl start ananicy-cpp
    sleep 2
    echo "Запуск служб - Nohang: Слежение за потребление ОЗУ, убивает процесс который начинает потреблять больше ОЗУ"
    systemctl enable --now nohang-desktop && systemctl start nohang-desktop
    sleep 2
    echo "Запуск служб - IrqBalance: Распределяет нагрузку на все ядра процессора"
    systemctl enable --now irqbalance && systemctl start IrqBalance
    sleep 2
    echo "Запуск служб - RNG-Tools: Следит за энтропией системы(более 1 секунды) аппаратными средствами"
    systemctl enable --now rngd && systemctl start rngd
    sleep 2
  fi
}

compilation_optimization(){
  echo "Выполняю настройку makepkg для компиляции программ..."

  if [[ $(grep 'CFLAGS="-march=native -mtune=native -O2' /etc/makepkg.conf) ]]; then
    echo "Флаги уже прописаны!"
  else
    makepkgconf="/etc/makepkg.conf"

    sed -i 's/-march=x86-64 -mtune=generic/-march=native -mtune=native/g' ${makepkgconf}

    sed -i '/LDFLAGS/d' ${makepkgconf}

    sed -i '/LTOFLAGS/d' ${makepkgconf}

    sed -i '/CXXFLAGS="$CFLAGS.*"/a RUSTFLAGS="-C opt-level=3"' ${makepkgconf}

    sed -i '/#MAKEFLAGS="-j2"/d' ${makepkgconf}

    sed -i '/RUSTFLAGS="-C opt-level=3"/a MAKEFLAGS="-j$(nproc) -l$(nproc)"' ${makepkgconf}

    sed -i '/MAKEFLAGS="-j$(nproc) -l$(nproc)"/a OPTIONS=(strip docs !libtool !staticlibs emptydirs zipman purge !debug !lto)' ${makepkgconf}

    grep -v "#" ${makepkgconf} | grep "BUILDENV=(.*)" | sed -i 's/!ccache/ccache/g' ${makepkgconf}
  fi

  echo "Вы хотите установить Clang и LLVM?
  1 - Yes
  Если не хотите, то не будут установлены
  2 - No"
  echo "Внимание! Использование Clang при сборке некоторых пакетов может вызывать ошибки при сборке!"

  echo -n "введите цифру: "
  read CLANG_LLVM

  if [[ $CLANG_LLVM == 1 ]]; then
    makepkgconf="/etc/makepkg.conf"

    $aur_install -Sy --needed clang llvm lld

    echo "Прописывание путей для компиляторов в конфигурации..."

    clang_variables_array=("export CC=clang" "export CXX=clang++" "export LD=ld.lld" "export CC_LD=lld" "export CXX_LD=lld" "export AR=llvm-ar" "export NM=llvm-nm" "export STRIP=llvm-strip" "export OBJCOPY=llvm-objcopy" "export OBJDUMP=llvm-objdump" "export READELF=llvm-readelf" "export RANLIB=llvm-ranlib" "export HOSTCC=clang" "export HOSTCXX=clang++" "export HOSTAR=llvm-ar" "export HOSTLD=ld.lld")

    for clang_variables in "${clang_variables_array[@]}"; do
      if [[ $(grep 'CHOST="x86_64-pc-linux-gnu"' /etc/makepkg.conf | grep "$clang_variables") ]]; then
        echo "Переменная $clang_variables уже прописана!"
      elif [[ $(grep 'CHOST="x86_64-pc-linux-gnu"' /etc/makepkg.conf | grep $clang_variables) != $clang_variables ]]; then
        sudo sed -i 's|CHOST="x86_64-pc-linux-gnu"|&\n'"$clang_variables"'|' ${makepkgconf}
        echo "Переменная $clang_variables удачно прописана!"
      fi
    done

    echo "Переменные прописаны!"
  fi
}

zram(){
      echo "Вы хотите установить ZRam?(Сжатие ОЗУ)
1 - Да
2 - Нет"
    echo -n "Введите цифру: "
    read ZRAM_SETUP
    if [[ "$ZRAM_SETUP" = 1 ]]; then
      echo "Установка пакетов..."
      sleep 2

      sudo pacman -Sy zram-generator

      echo "Пакет установлен! Идёт настройка ZRam..."

      # Добавление строки zram в конфигурационный файл /etc/modules-load.d/zram.conf
      if [[ $(grep 'zram' /etc/modules-load.d/zram.conf) ]]; then
        echo "Строка zram уже прописана!"
      else
        sudo sed -i 's|zram|' /etc/modules-load.d/zram.conf
        echo "Строка zram прописана!"
      fi

      # Добавление строки options zram num_devices=2 в конфигурационный файл /etc/modprobe.d/zram.conf
      if [[ $(grep 'options zram num_devices=2' /etc/modprobe.d/zram.conf) ]]; then
        echo "Строка options zram num_devices=2 уже прописана!"
      else
        sudo sed -i 's|options zram num_devices=2|' /etc/modprobe.d/zram.conf
        echo "Строка options zram num_devices=2 прописана!"
      fi

      zram_rules_array=('"KERNEL=="zram0", ATTR{disksize}="2048M" RUN="/usr/bin/mkswap /dev/zram0", TAG+="systemd"' '"KERNEL=="zram1", ATTR{disksize}="2048M" RUN="/usr/bin/mkswap /dev/zram1", TAG+="systemd"')

      # Цикл для добавления строк в конфигурационный файл /etc/udev/rules.d/zram.rules из массива zram_rules_array
      for zram_rules in "${zram_rules_array[@]}"; do
        if [[ $(grep 'KERNEL=="zram0"' /etc/udev/rules.d/zram.rules | grep "$zram_rules") ]]; then
            echo "Строка $zram_rules уже прописана!"
        elif [[ $(grep 'KERNEL=="zram0"' /etc/udev/rules.d/zram.rules | grep $zram_rules) != $zram_rules ]]; then
            sudo sed -i 's|KERNEL=="zram0"|&\n'"$zram_rules"'|' /etc/udev/rules.d/zram.rules
            echo "Строка $zram_rules прописана!"
        fi
      done
    fi
}

fonts(){
  echo "Настройка сглаживания шрифтов"
  sudo sed '12s/^#//' -i /etc/profile.d/freetype2.sh
  cd /home/$USER/Загрузки/
  wget https://cdn.discordapp.com/attachments/981462916375121920/982639394114666567/local.conf
  sudo mv local.conf /etc/fonts/
  echo -e "Xft.dpi: 96
  Xft.antialias: true
  Xft.hinting: true
  Xft.rgba: rgb
  Xft.autohint: false
  Xft.hintstyle: hintslight
  Xft.lcdfilter: lcddefault" >> ~/.Xresources
}

audio(){
  echo "Вы хотите установить звуковой сервер pipewire?
1) Да
Если нет, то установится звуковой сервер Pulseaudio
2) Нет"
  echo -n "Введите цифру: "
  read SOUND_SERVERS
  if [[ "$SOUND_SERVERS" == 1 ]]; then
    echo "Какой менеджер сеансов pipewire вы хотите установить?
1 - pipewire-media-session
2 - Wireplumber"
    echo -n "Введите цифру: "
    read PIPEWIRE_SESSION
    if [[ "$PIPEWIRE_SESSION" = 1 ]]; then
      sudo pacman -S --needed pipewire pipewire-media-session pipewire-pulse pipewire-docs pipewire-alsa pipewire-jack lib32-pipewire lib32-pipewire-jack libpulse lib32-libpulse xdg-desktop-portal
      systemctl enable pipewire-media-session
      systemctl --user enable --now pipewire.service
    elif [[ "$PIPEWIRE_SESSION" = 2 ]]; then
        sudo pacman -S --needed pipewire wireplumber pipewire-pulse pipewire-docs pipewire-alsa pipewire-jack lib32-pipewire lib32-pipewire-jack libpulse lib32-libpulse xdg-desktop-portal
        systemctl enable wireplumber
        systemctl --user enable --now pipewire.service
    fi
  elif [[ "$SOUND_SERVERS" == 2 ]]; then
    sudo pacman -S pulseaudio pulseaudio-alsa alsa-utils lib32-libpulse xdg-desktop-portal
    pulseaudio -D
  fi
}

ZSH(){
  echo "Вы хотите установить zsh и Oh-My-Zsh?
1) Да
Если нет, то не будет установлен
2) Нет"
  echo -n "Введите цифру: "
  read ZSH_INSTALL
  if [[ "$ZSH_INSTALL" == 1 ]]; then
    echo "Установка ZSH..."
    sleep 2

    sudo pacman -Sy zsh
    sleep 2

    echo "Установка Oh-My-Zsh..."
    curl -L https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh | sh
    chsh -s /bin/zsh

    sleep 2

    echo "Установка zsh-syntax-highlighing и zsh-autosuggestions"
    sleep 2

    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

    echo "zsh-syntax-highlighting удачно загружен!"
    sleep 5

    git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

    echo "zsh-autosuggestion удачно загружен!"
    sleep 5

    echo "Установка плагинов..."

    sed -i 's/plugins=(git)/plugins=(git zsh-syntax-highlighting zsh-autosuggestions)/g' /home/$USER/.zshrc
    sleep 2

    echo "Плагины удачно установлены!"

    echo "Вы хотите установить тему powerlevel10k?
    1 - Да
    2 - Нет"
    echo -n "Введите цифру: "

    read POWERLEVEL_INSTALL

      if [[ "$POWERLEVEL_INSTALL" = 1 ]]; then
          cd ~
          mkdir ~/.p10k_fonts_tmp && cd ~/.p10k_fonts_tmp
        
          echo "Идёт загрузка шрифтов..."

          echo "Создание временных каталогов"
          mkdir ~/.p10k_fonts_tmp
          cd ~/.p10k_fonts_tmp

          echo "Загрузка шрифтов..."
          wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf

          echo "Шрифты удачно загружены"
          
          cp MesloLGS\ NF\ Bold.ttf MesloLGS\ NF\ Regular.ttf MesloLGS\ NF\ Italic.ttf MesloLGS\ NF\ Bold\ Italic.ttf /home/$USER/.local/share/fonts

          echo "Шрифты удачно установлены!"

          echo "Удаление временного каталога..."

          cd ~
          rm -rf ~/.p10k_fonts_tmp

          echo "Временный каталог успешно удалён!"

          echo "Установка темы..."
        
          git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

          sed -i 's|ZSH_THEME="*"|ZSH_THEME="powerlevel10k/powerlevel10k"|' /home/$USER/.zshrc
          

          echo "Не забудьте указать шрифт в настройках терминала. И также перезапустите терминал как скрипт закончит свою работу"
      fi
  fi
}

browser(){
  echo "Какой браузер вы хотите установить?
1) Firefox
2) Google Chrome
3) Yandex Browser"
  echo -n "Введите цифру: "
  read BROWSER
  if [[ "$BROWSER" == 1 ]]; then
    sudo pacman -Sy firefox firefox-i18n-ru
    elif [[ "$BROWSER" == 2 ]]; then
      sudo pacman -Sy google-chrome
      elif [[ "$BROWSER" == 3 ]]; then # Яндекс, горите в аду со своими зависимостями
        sudo pacman -Sy debtap zenity
        echo "Файл будет загружен в папке загрузки"
        sleep 3
        cd /home/$USER/Загрузки/
        wget -O Yandex.deb https://repo.yandex.ru/yandex-browser/deb/pool/main/y/yandex-browser-stable/yandex-browser-stable_22.5.0.1878-1_amd64.deb
        sleep 25
        DEB_COMPILATION=$(zenity --file-selection --file-filter="*.deb" --title="Select the file to compile" --filename="${USER}")
        if [ $? -eq 1 ]; then
            exit 1;
        fi
        sudo debtap -u
        sudo debtap -Q "${DEB_COMPILATION}"
        PKG_INSTALL=$(zenity --file-selection --file-filter="*.pkg.tar.zst" --title="Select the file to install" --filename="${USER}")
        if [ $? -eq 1 ]; then
            exit 1;
        fi
        pkexec /bin/bash -c "sudo pacman -U --noconfirm ${PKG_INSTALL}"
  fi
}

video_editing(){
  echo "Хотите установить софт для стриминга и монтажа?
1) Да
2) Нет"
  echo -n "Введите цифру: "
  read VIDEO
  if [[ "$VIDEO" == 1 ]]; then
    sudo pacman -Sy kdenlive gimp breeze breeze-gtk breeze-icons obs-studio
  fi
}

wine(){
  echo "Что вы выберете для запуска Windows игр на Linux?
1) PortWINE
2) StartWine"
  echo -n "Введите цифру: "
  read GAMES
  if [[ "$GAMES" == 1 ]]; then
    sudo pacman -Syu bash icoutils wget bubblewrap zstd cabextract bc tar openssl gamemode desktop-file-utils curl dbus freetype2 gdk-pixbuf2 ttf-font zenity lsb-release nss xorg-xrandr vulkan-driver vulkan-icd-loader lsof lib32-freetype2 lib32-libgl lib32-gcc-libs lib32-libx11 lib32-libxss lib32-alsa-plugins lib32-libgpg-error lib32-nss lib32-vulkan-driver lib32-vulkan-icd-loader lib32-gamemode lib32-openssl
    wget -c "https://github.com/Castro-Fidel/PortWINE/raw/master/portwine_install_script/PortProton_1.0" && sh PortProton_1.0 -y
  elif [[ "$GAMES" == 2 ]]; then
    cd /home/$USER/Загрузки/
    wget https://cdn.discordapp.com/attachments/858478894989312040/978765476941414420/StartWine_v355
    chmod +x StartWine_v355 && sh StartWine_v355
    sleep 5
  fi
}

java(){
  echo "Вы хотите установить последнюю версию java (openjdk)?
1) Да
2) Нет"
  echo -n "Введите цифру: "
  read JAVA
  if [[ "$JAVA" == 1 ]]; then
    sudo pacman -Sy jdk-openjdk jre-openjdk
  fi
}

pamac(){
  echo "Вы хотите установить графический pamac (pacman)?
1) Да
Если нет, то не будет установлен
2) Нет"
  echo -n "Введите цифру: "
  read PAMAC_INSTALL
  if [[ "$PAMAC_INSTALL" == 1 ]] && [[ "$AUR_HELPER" == 1 ]] ; then
    yay -S pamac-aur archlinux-appstream-data
  elif [[ "$PAMAC_INSTALL" == 1 ]] && [[ "$AUR_HELPER" == 2 ]] ; then
    pikaur -S pamac-aur archlinux-appstream-data
  fi
}

additional_package_manager(){
  echo "Что вы хотите установить?
1) flatpak
2) appimage
3) Ничего не устанавливать"
  echo -n "Введите цифру: "
  read BOX
  if [[ "$BOX" == 1 ]]; then
    sudo pacman -Sy flatpak flatpak-xdg-utils
  elif [[ "$BOX" == 2 ]]; then
    sudo pacman -Sy appimagepool libappimage
  fi
}

reboot(){
  echo "Вы хотите перезагрузить систему?
Y - Да
N - Нет"
  echo -n "Укажите свой выбор: "
  read SYSTEM_REBOOT
  if [[ "$SYSTEM_REBOOT" = Y ]]; then
    reboot
  elif [[ "$SYSTEM_REBOOT" = N ]]; then
    exit
  fi
}

#clearing and triggering the selection
clear

echo "Version 1.7.4
░██████╗███████╗████████╗██╗░░░██╗██████╗░
██╔════╝██╔════╝╚══██╔══╝██║░░░██║██╔══██╗
╚█████╗░█████╗░░░░░██║░░░██║░░░██║██████╔╝
░╚═══██╗██╔══╝░░░░░██║░░░██║░░░██║██╔═══╝░
██████╔╝███████╗░░░██║░░░╚██████╔╝██║░░░░░
╚═════╝░╚══════╝░░░╚═╝░░░░╚═════╝░╚═╝░░░░░
"

echo "Запустить весь скрипт или выбрать отдельный модуль?
1) Запустить весь скрипт
2) Выбрать отдельный модуль/модули
3) Выйти"
echo -n "
Введите цифру: "

read TEXT
if [[ "$TEXT" == 1 ]]; then
  key_updates 
  system_update
  repositories
  editors
  soft
  package_manager
  NVIDIA
  kernel
  DE
  services
  compilation_optimization
  zram
  fonts
  audio
  ZSH
  browser
  video_editing
  wine
  java
  pamac
  additional_package_manager
  reboot
  elif [[ "$TEXT" == 2 ]]; then
  clear
  PS3="
Выберите операцию: "
select start in Обновить\ Ключи Обновление\ Системы Настройка\ Репозиториев Установка\ Редактора Установка\ Нужных\ Приложений Установка\ Пакетов Установка\ Драйверов\ NVIDIA Установка\ Ядра Настройка\ DE Настройка\ Служб Настройка\ Установка Zram\ Оптимизация\ Сборки Настройка\ Шрифтов Настройка\ Аудио Установка\ Менеджера\ ZSH Установка\ Браузера Приложения\ Для\ Видео-Монтажа Установка\ PortWINE\ или\ StartWine Установка\ JAVA Установка\ pamac Дополнительный\ Менеджер\ Пакетов Перезагрузка; do

  case $start in
    Обновить\ Ключи)
    key_updates
    cas
    ;;
  Обновление\ Системы)
    system_update
    cas
    ;;
  Настройка\ Репозиториев)
    repositories
    cas
    ;;
  Установка\ Редактора)
    editors
    cas
    ;;
  Установка\ Нужных\ Приложений)
    soft
    cas
    ;;
  Установка\ Менеджера\ Пакетов)
    package_manager
    cas
    ;;
  Установка\ Драйверов\ NVIDIA)
    NVIDIA
    cas
    ;;
  Установка\ Ядра)
    kernel
    cas
    ;;
  Настройка\ DE)
    DE
    cas
    ;;
  Настройка\ Служб)
    services
    cas
    ;;

  Оптимизация\ Сборки)
    compilation_optimization
    cas
    ;;

  Установка\ Zram)
    zram
    cas
    ;;
  Настройка\ Шрифтов)
    fonts
    cas
    ;;
  Настройка\ Аудио)
    audio
    cas
    ;;
  Установка\ ZSH)
    ZSH
    cas
    ;;
  Установка\ Браузера)
    browser
    cas
    ;;
  Приложения\ Для\ Видео-Монтажа)
    video_editing
    cas
    ;;
  Установка\ PortWINE\ или\ StartWine)
    wine
    cas
    ;;
  Установка\ JAVA)
    java
    cas
    ;;
  Установка\ pamac)
    pamac
    cas
    ;;
  Дополнительный\ Менеджер\ Пакетов)
    additional_package_manager
    cas
    ;;
  Перезагрузка)
    reboot
    ;;
  Quit)
    break
    ;;
    *) 
      echo "Некорректная операция '$REPLY'";;
  esac
done
  elif [[ "$TEXT" == 3 ]]; then
  exit
fi